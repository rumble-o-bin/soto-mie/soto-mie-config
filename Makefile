.PHONY: help
help:  ## Show help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

install_argocd: ## install argocd
	@helm upgrade --install -n argocd --create-namespace argocd 0-bootstrap/argo-cd/ -f 0-bootstrap/argo-cd/values.yaml

get_init_secret: ## get argocd init secret
	@kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d

forward_service: ## forward argocd forward service
	@kubectl port-forward -n argocd svc/argocd-server 8080:80

minio:
	@helm install minio -n minio oci://registry-1.docker.io/bitnamicharts/minio --create-namespace
