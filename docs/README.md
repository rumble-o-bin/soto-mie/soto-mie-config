# Docs

# Table of Contents

[[_TOC_]]

## Structure

```bash
.
├── 1-management/
│   ├── environments/
│   │   ├── staging/
│   │   │   ├── app-project.yaml
│   │   │   └── application-set.yaml # -- pointing to ./overlays/staging
│   │   └── test/
│   │       ├── app-project.yaml
│   │       └── application-set.yaml # -- pointing to ./overlays/test
│   ├── root/
│   │   ├── staging-apps.yaml # -- pointing to ./environments/staging
│   │   ├── test-apps.yaml # -- pointing to ./environments/test
│   │   └── infrastructure.yaml # -- pointing to ./infra
│   ├── root-project.yaml # -- pointing to ./root
│   └── argo-cd.yaml # -- pointing to argocd itself
├── applications # -- act as bases applications
│   ├── app-1/
│   ├── app-2/
│   └── ...
├── overlays/
│   ├── staging/
│   │   ├── app-1/
│   │   └── app-2/
│   └── test/
└── infra/
    ├── data/
    └── utility/
```

## Run

```bash
kubectl apply -f root-project.yaml
kubectl apply -f argocd.yaml
```